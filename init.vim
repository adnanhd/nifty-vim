inoremap <M-Left> <Home>
inoremap <M-Right> <End>
nnoremap <M-Left> _
nnoremap <M-Right> $

inoremap <M-Up> <C-o>:m-2<CR>
inoremap <M-Down> <C-o>:m+1<CR>
nnoremap <M-Up> :m-2<CR>
nnoremap <M-Down> :m+1<CR>

inoremap <C-Up> <C-o><C-y>
inoremap <C-Down> <C-o><C-e>
nnoremap <C-Up> <C-y>
nnoremap <C-Down> <C-e>

inoremap <M-.> <C-o>gt
inoremap <M-,> <C-o>gT
nnoremap <M-.> gt
nnoremap <M-,> gT

nnoremap tt :tabe<CR>

inoremap <M-Backspace> <C-w>
inoremap <C-U> <C-o>u

let g:vimspector_enable_mappings = 'VISUAL_STUDIO'

" disable arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>

call plug#begin('~/.local/share/nvim/plugged')
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'ap/vim-css-color'
Plug 'sainnhe/sonokai'
Plug 'jeffkreeftmeijer/vim-numbertoggle'

Plug 'fatih/vim-go'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-fugitive'
Plug 'https://github.com/kevinoid/vim-jsonc'
Plug 'easymotion/vim-easymotion'
Plug 'jackguo380/vim-lsp-cxx-highlight'

Plug 'puremourning/vimspector'

Plug 'ayu-theme/ayu-vim'
Plug 'karb94/neoscroll.nvim'
Plug 'folke/zen-mode.nvim'
Plug 'junnplus/nvim-lsp-setup'
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'
Plug 'tribela/vim-transparent'
"Plug 'nvim-treesitter/nvim-treesitter', {'do': 'TSUpdate'}
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'gfanto/fzf-lsp.nvim'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'
Plug 'jaxbot/semantic-highlight.vim'
Plug 'tomasiser/vim-code-dark'
Plug '907th/vim-auto-save'
Plug 'jiangmiao/auto-pairs'
call plug#end()
" Remember cursor position
augroup vimrc-remember-cursor-position
    autocmd!
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

" nerdtree settings
map <F2> :NERDTreeToggle<CR>
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <F2> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
nnoremap <F3> :nohl<CR>

let NERDTreeWinSize=32
let NERDTreeWinPos="left"
let NERDTreeShowHidden=1
let NERDTreeAutoDeleteBuffer=1
let NERDTreeAutoDeleteBuffer=1

" Set
set termguicolors number nowrap autoindent
set tabstop=4 softtabstop=4 noexpandtab shiftwidth=4 smarttab

nmap <space>e <Cmd>CocCommand explorer<CR>

" Airline

let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" Theme

let g:sonokai_style = 'shusia'
colorscheme sonokai
set termguicolors
au ColorScheme * hi Normal ctermbg=NONE
au ColorScheme * hi LineNr ctermbg=NONE
au ColorScheme * hi SignColumn ctermbg=NONE
au ColorScheme * hi NonText ctermbg=NONE

hi Normal ctermbg=None
hi LineNr ctermbg=None
hi SignColumn ctermbg=None
hi NonText ctermbg=None

let ayucolor="dark"
colorscheme codedark

lua require("zen-mode").setup{}

lua << EOF
require('nvim-lsp-setup').setup{
	servers = {
		pylsp = {}
	}
}
EOF

set completeopt=menu,menuone,noselect

lua <<EOF
  -- Setup nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
      end,
    },
    window = {
      -- completion = cmp.config.window.bordered(),
      -- documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = true }),
      ['<Tab>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      { name = 'vsnip' }, -- For vsnip users.
      -- { name = 'luasnip' }, -- For luasnip users.
      -- { name = 'ultisnips' }, -- For ultisnips users.
      -- { name = 'snippy' }, -- For snippy users.
    }, {
      { name = 'buffer' },
    })
  })

  -- Set configuration for specific filetype.
  cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
      { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
      { name = 'buffer' },
    })
  })

  -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline('/', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    })
  })

  -- Setup lspconfig.
  local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  require('lspconfig')['clangd'].setup {
    capabilities = capabilities
  }
EOF
" colorscheme happy_hacking
